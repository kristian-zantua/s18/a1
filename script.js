//3 & 4
let trainer = {
	name: `Ash Ketchum`,
	age: 10,
	pokemon: [`Pikachu`, `Squirtle`, `Bulbasaur`, `Charizard`],
	friends: {
		hoenn:[`May`, `Max`],
		kanto:[`Brock`, `Misty`]
	},
//5	
	talk: function(){
		console.log(`Pikachu, I choose you!`)
	}
}
//6
console.log(trainer);

console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.pokemon);
console.log(trainer.friends);
//7
trainer.talk();
//8
function pokemon(name, lvl){
	this.name = name,
	this.level = lvl,
	this.health = lvl * 2,
	this.attack = lvl
//10	
	this.tackle = function(target){

		let currentHP = target.health - this.attack

		console.log(`${this.name} tackled ${target.name}`)

		target.health = currentHP

		if (target.health <=0){
			console.log(`health is now reduced to 0`);
				target.faint(target)
		}
		if (target.health >0){
			console.log(`health is now reduced to ${currentHP}`)
		}
		//return target

	}
	this.faint = function(target){
		console.log(`${target.name} fainted!`)
	}

}
//9
let pikachu = new pokemon(`Pikachu`, 12);
console.log(pikachu);

let geodude = new pokemon(`Geodude`,8);
console.log(geodude);

let mewtwo = new pokemon(`Mewtwo`,100);
console.log(mewtwo);
//
geodude.tackle(pikachu);

mewtwo.tackle(geodude);